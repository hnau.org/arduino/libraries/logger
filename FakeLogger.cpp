#include "FakeLogger.h"

FakeLogger* FakeLogger::instance = nullptr;

FakeLogger& FakeLogger::getInstance() {
    if (instance == nullptr) {
        instance = new FakeLogger();
    }
    return *instance;
}

FakeLogger::FakeLogger() {}

size_t FakeLogger::log(CharsProvider charsProvider) {
    return 0;
}

size_t FakeLogger::log(char value) {
    return 0;
}

size_t FakeLogger::log(unsigned char value, int radix) {
    return 0;
}

size_t FakeLogger::log(int value, int radix) {
    return 0;
}

size_t FakeLogger::log(unsigned int value, int radix) {
    return 0;
}

size_t FakeLogger::log(long value, int radix) {
    return 0;
}

size_t FakeLogger::log(unsigned long value, int radix) {
    return 0;
}

size_t FakeLogger::log(double value, int accuracy) {
    return 0;
}

size_t FakeLogger::log(const Printable& value) {
    return 0;
}
