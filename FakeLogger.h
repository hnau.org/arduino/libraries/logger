#ifndef FAKELOGGER_H
#define FAKELOGGER_H

#include "Logger.h"


class FakeLogger: public Logger {

private:
    static FakeLogger* instance;

    FakeLogger();

public:

    static FakeLogger& getInstance();

    virtual size_t log(CharsProvider charsProvider) override;

    virtual size_t log(char) override;

    virtual size_t log(unsigned char, int = DEC) override;

    virtual size_t log(int, int = DEC) override;

    virtual size_t log(unsigned int, int = DEC) override;

    virtual size_t log(long, int = DEC) override;

    virtual size_t log(unsigned long, int = DEC) override;

    virtual size_t log(double, int = 2) override;

    virtual size_t log(const Printable&) override;

};


#endif //FAKELOGGER_H
