#ifndef LOGGER_H
#define LOGGER_H

#include <Arduino.h>
#include <CharsProvider.h>

class Logger {

public:

    virtual size_t log(CharsProvider charsProvider) = 0;

    virtual size_t log(char) = 0;

    virtual size_t log(unsigned char, int = DEC) = 0;

    virtual size_t log(int, int = DEC) = 0;

    virtual size_t log(unsigned int, int = DEC) = 0;

    virtual size_t log(long, int = DEC) = 0;

    virtual size_t log(unsigned long, int = DEC) = 0;

    virtual size_t log(double, int = 2) = 0;

    virtual size_t log(const Printable&) = 0;

};


#endif //LOGGER_H
