#include "SerialLogger.h"

SerialLogger::SerialLogger(uint32_t speed) {
    Serial.begin(speed);
}

size_t SerialLogger::log(CharsProvider charsProvider) {
	size_t count = 0;
	charsProvider([&](char ch) {
		count += Serial.print(ch);
	});
	return count;
}

size_t SerialLogger::log(char value) {
    return Serial.print(value);
}

size_t SerialLogger::log(unsigned char value, int radix) {
    return Serial.print(value, radix);
}

size_t SerialLogger::log(int value, int radix) {
    return Serial.print(value, radix);
}

size_t SerialLogger::log(unsigned int value, int radix) {
    return Serial.print(value, radix);
}

size_t SerialLogger::log(long value, int radix) {
    return Serial.print(value, radix);
}

size_t SerialLogger::log(unsigned long value, int radix) {
    return Serial.print(value, radix);
}

size_t SerialLogger::log(double value, int accuracy) {
    return Serial.print(value, accuracy);
}

size_t SerialLogger::log(const Printable& value) {
    return Serial.print(value);
}
