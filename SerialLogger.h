#ifndef SERIALLOGGER_H
#define SERIALLOGGER_H

#include "Logger.h"

class SerialLogger: public Logger {

public:

    SerialLogger(uint32_t speed);

    virtual size_t log(CharsProvider charsProvider) override;

    virtual size_t log(char) override;

    virtual size_t log(unsigned char, int = DEC) override;

    virtual size_t log(int, int = DEC) override;

    virtual size_t log(unsigned int, int = DEC) override;

    virtual size_t log(long, int = DEC) override;

    virtual size_t log(unsigned long, int = DEC) override;

    virtual size_t log(double, int = 2) override;

    virtual size_t log(const Printable&) override;

};


#endif //SERIALLOGGER_H
