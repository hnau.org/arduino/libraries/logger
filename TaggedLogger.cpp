#include "TaggedLogger.h"
#include <TimeUtils.h>

TaggedLogger::TaggedLogger(
	Logger& logger_,
	CharsProvider tag
) : logger(logger_) {
    this->tag = tag;
}

void TaggedLogger::log(
        std::function<void(Logger&)> logAction
) {
    logger.log(CP(TimeUtils::millisecondsToString(millis())));
    logger.log(FCP(" ["));
    logger.log(tag);
    logger.log(FCP("] "));
    logAction(logger);
    logger.log(FCP("\n"));
}
