#ifndef TAGGEDLOGGER_H
#define TAGGEDLOGGER_H

#include "Logger.h"
#include "CharsProvider.h"

class TaggedLogger {

private:
    Logger& logger;
    CharsProvider tag;

public:

    TaggedLogger(
            Logger& logger,
            CharsProvider tag
    );
    
    void log(
        std::function<void(Logger&)> logAction
	);

};


#endif //TAGGEDLOGGER_H
